const Mail = require('../services/Mail')

const template = function (ad, user, content) {
  return `<html>
  <head>
    <style>
      body {
        font-family: Arial, Helvetica, sans-serif
      }
    </style>
  </head>
  <body>
    <strong>Olá, ${ad.author.name}</strong>
    <p>Você tem uma nova solicitação de compra para o anúncio: ${ad.title}</p>
    <br>
    <strong>${user.name} (${user.email}):</strong>
    <p>${content}</p>
  </body>
  </html>`
}

class PurchaseMail {
  get key () {
    return 'PurchaseMail'
  }

  async handle (job, done) {
    const { ad, user, content } = job.data

    await Mail.sendMail({
      from: '"Calebe Machado" <calebe@clsax.com.br>',
      to: ad.author.email,
      subject: `Solicitação de Compra: ${ad.title}`,
      html: template(ad, user, content)
    })

    return done()
  }
}

module.exports = new PurchaseMail()
